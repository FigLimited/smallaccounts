<?php

return [
    'nominalAccounts' => [
        'presets' => [
            'salesLedger' => env('SALES_LEDGER', 6),
            'purchaseLedger' => env('PURCHASE_LEDGER', 7)
        ]
    ]
];
