<?php

namespace App\Http\Controllers;

use App\Models\SalesLedger;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class SalesLedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Inertia::render('SalesLedgers/Index', [
            'salesLedgers' => SalesLedger::query()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalesLedger  $salesLedger
     * @return \Illuminate\Http\Response
     */
    public function show(SalesLedger $salesLedger)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SalesLedger  $salesLedger
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesLedger $salesLedger)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalesLedger  $salesLedger
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesLedger $salesLedger)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalesLedger  $salesLedger
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesLedger $salesLedger)
    {
        //
    }
}
