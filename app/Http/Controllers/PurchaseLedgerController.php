<?php

namespace App\Http\Controllers;

use App\Models\PurchaseLedger;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PurchaseLedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('PurchaseLedgers/Index', [
            'purchaseLedgers' => PurchaseLedger::query()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PurchaseLedger  $purchaseLedger
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseLedger $purchaseLedger)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PurchaseLedger  $purchaseLedger
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseLedger $purchaseLedger)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PurchaseLedger  $purchaseLedger
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseLedger $purchaseLedger)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PurchaseLedger  $purchaseLedger
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseLedger $purchaseLedger)
    {
        //
    }
}
