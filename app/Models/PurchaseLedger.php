<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseLedger extends Model
{
    use HasFactory;

    protected $appends = [
        'amount'
    ];

    public function purchase_ledger_lines()
    {
        return $this->hasMany(PurchaseLedgerLine::class);
    }

    public function getAmountAttribute()
    {
        $amount = 0;

        foreach($this->purchase_ledger_lines as $purchaseLedgerLine) {
            foreach($purchaseLedgerLine->nominal_header->nominal_transactions as $nominalTransaction) {
                if($nominalTransaction->credit_nominal_account_id == config('smallaccounts.nominalAccounts.presets.purchaseLedger')) {
                    $amount += $nominalTransaction->amount;
                }
                if($nominalTransaction->debit_nominal_account_id == config('smallaccounts.nominalAccounts.presets.purchaseLedger')) {
                    $amount -= $nominalTransaction->amount;
                }
            }
        }
        return $amount;
    }
}
