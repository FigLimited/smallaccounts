<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesLedger extends Model
{
    use HasFactory;

    protected $appends = [
        'amount'
    ];

    public function sales_ledger_lines()
    {
        return $this->hasMany(SalesLedgerLine::class);
    }

    public function getAmountAttribute()
    {
        $amount = 0;

        foreach($this->sales_ledger_lines as $salesLedgerLine) {
            foreach($salesLedgerLine->nominal_header->nominal_transactions as $nominalTransaction) {
                if($nominalTransaction->debit_nominal_account_id == config('smallaccounts.nominalAccounts.presets.salesLedger')) {
                    $amount += $nominalTransaction->amount;
                }
                if($nominalTransaction->credit_nominal_account_id == config('smallaccounts.nominalAccounts.presets.salesLedger')) {
                    $amount -= $nominalTransaction->amount;
                }
            }
        }
        return $amount;
    }
}
