<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesLedgerLine extends Model
{
    use HasFactory;

    public function sales_ledger()
    {
        return $this->belongsTo(SalesLedger::class);
    }

    public function nominal_header()
    {
        return $this->morphOne(NominalHeader::class, 'headerable');
    }
}
