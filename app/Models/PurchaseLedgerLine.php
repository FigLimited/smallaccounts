<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseLedgerLine extends Model
{
    use HasFactory;

    public function purchase_ledger()
    {
        return $this->belongsTo(PurchaseLedger::class);
    }

    public function nominal_header()
    {
        return $this->morphOne(NominalHeader::class, 'headerable');
    }
}
