<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NominalHeader extends Model
{
    public function headerable()
    {
        return $this->morphTo();
    }

    public function nominal_transactions()
    {
        return $this->hasMany(NominalTransaction::class);
    }
}
