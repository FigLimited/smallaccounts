<?php

namespace Database\Seeders;

use App\Models\PurchaseLedger;
use App\Models\SalesLedger;
use App\Models\SalesLedgerLine;
use Illuminate\Database\Seeder;

use App\Models\NominalAccount;
use App\Models\NominalHeader;
use App\Models\NominalTransaction;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        $sales = NominalAccount::create([
            'name' => 'Sales',
            'default' => 'credit'
        ]);
        $salesDiscount = NominalAccount::create([
            'name' => 'Sales Discount',
            'default' => 'debit'
        ]);
        NominalAccount::create([
            'name' => 'Stock'
        ]);
        $rent = NominalAccount::create([
            'name' => 'Rent'
        ]);
        NominalAccount::create([
            'name' => 'Telephone'
        ]);
        $salesLedgerNominal = NominalAccount::create([
            'name' => 'Sales Ledger'
        ]);
        $purchaseLedgerNominal = NominalAccount::create([
            'name' => 'Purchase Ledger',
            'default' => 'credit'
        ]);
        $bank = NominalAccount::create([
            'name' => 'Bank Balance'
        ]);
        $vat = NominalAccount::create([
            'name' => 'VAT',
            'default' => 'credit'
        ]);

        $nominalHeader = NominalHeader::create([
            'accounted_at' => now(),
        ]);
        NominalTransaction::create([
            'nominal_header_id' => $nominalHeader->id,
            'accounted_at' => now(),
            'amount' => 120,
            'debit_nominal_account_id' => $bank->id,
            'credit_nominal_account_id' => $sales->id,
        ]);
        NominalTransaction::create([
            'nominal_header_id' => $nominalHeader->id,
            'accounted_at' => now(),
            'amount' => 20,
            'debit_nominal_account_id' => $sales->id,
            'credit_nominal_account_id' => $vat->id,
        ]);


        $nominalHeader2 = NominalHeader::create([
            'accounted_at' => now(),
        ]);
        NominalTransaction::create([
            'nominal_header_id' => $nominalHeader2->id,
            'accounted_at' => now(),
            'amount' => 360,
            'debit_nominal_account_id' => $bank->id,
            'credit_nominal_account_id' => $sales->id,
        ]);
        NominalTransaction::create([
            'nominal_header_id' => $nominalHeader2->id,
            'accounted_at' => now(),
            'amount' => 60,
            'debit_nominal_account_id' => $sales->id,
            'credit_nominal_account_id' => $vat->id,
        ]);
        // NominalTransaction::create([
        //     'accounted_at' => now(),
        //     'amount' => 200,
        //     'debit_nominal_account_id' => $rent->id,
        //     'credit_nominal_account_id' => $bank->id,
        // ]);


        $salesInvoice = SalesLedger::query()->create([]);
        $salesInvoiceLine = $salesInvoice->sales_ledger_lines()->create([]);
        $salesInvoiceLineNominalHeader = $salesInvoiceLine->nominal_header()->create([
            'accounted_at' => now(),
        ]);
        $salesInvoiceLineNominalHeader->nominal_transactions()->create([
            'accounted_at' => now(),
            'amount' => 550,
            'debit_nominal_account_id' => $salesLedgerNominal->id,
            'credit_nominal_account_id' => $sales->id,
        ]);
        $salesInvoiceLineNominalHeader->nominal_transactions()->create([
            'accounted_at' => now(),
            'amount' => 50,
            'debit_nominal_account_id' => $salesDiscount->id,
            'credit_nominal_account_id' => $salesLedgerNominal->id,
        ]);
        $salesInvoiceLineNominalHeader->nominal_transactions()->create([
            'accounted_at' => now(),
            'amount' => 100,
            'debit_nominal_account_id' => $salesLedgerNominal->id,
            'credit_nominal_account_id' => $vat->id,
        ]);

//        $purchaseInvoice = PurchaseLedger::query()->create([]);
//        $purchaseInvoice->purchase_ledger_lines()->create([]);

        $purchaseInvoice = PurchaseLedger::query()->create([]);
        $purchaseInvoiceLine = $purchaseInvoice->purchase_ledger_lines()->create([]);
        $purchaseInvoiceLineNominalHeader = $purchaseInvoiceLine->nominal_header()->create([
            'accounted_at' => now(),
        ]);
        $purchaseInvoiceLineNominalHeader->nominal_transactions()->create([
            'accounted_at' => now(),
            'amount' => 200,
            'debit_nominal_account_id' => $rent->id,
            'credit_nominal_account_id' => $purchaseLedgerNominal->id,
        ]);
    }
}
